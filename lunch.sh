#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

# Get user information
res="n"
while test $res != "o"
do
    echo -n "Mot de passe du super utilisateur (root) MySQL: "
    read mysql_root_pass
    echo -n "Utilisateur SQL pour la base de données des mails: "
    read mysql_mail_user
    echo -n "Mot de passe pour l'utilisateur SQL $mysql_mail_user: "
    read mysql_mail_pass
    echo -n "Votre domaine (exemple: votredomaine.fr): "
    read domain
    echo -n "Host MX (exemple: mail): "
    read host
    echo "User MySQL: $mysql_mail_user"
    echo "Domaine: $domain"
    echo "Host: $host"
    echo "Serveur mail: $host.$domain"
    echo -n "Ces informations sont-elles exactes ? [o/n]: "
    read res
done

# Modify hostname
hostnamectl set-hostname $domain

# Install package
echo "Installation de postfix et de dovecot."
apt-get -y -qq install postfix postfix-mysql dovecot-core dovecot-imapd dovecot-pop3d dovecot-lmtpd dovecot-mysql

# Create mailserver db
echo "Création de la base de données"
mysqladmin --password="$mysql_root_pass" create mailserver
echo "GRANT SELECT ON mailserver.* TO '$mysql_mail_user'@'127.0.0.1' IDENTIFIED BY '$mysql_mail_pass';
GRANT SELECT ON mailserver.* TO '$mysql_mail_user'@'localhost' IDENTIFIED BY '$mysql_mail_pass';
FLUSH PRIVILEGES;" > grant.sql
mysql --password="$mysql_root_pass" < grant.sql
rm grant.sql

# Create tables
echo "Création des tables"
echo "CREATE TABLE IF NOT EXISTS \`virtual_domains\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`name\` varchar(50) NOT NULL,
  PRIMARY KEY (\`id\`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS \`virtual_users\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`domain_id\` int(11) NOT NULL,
  \`password\` varchar(106) NOT NULL,
  \`email\` varchar(100) NOT NULL,
  PRIMARY KEY (\`id\`),
  UNIQUE KEY \`email\` (\`email\`),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS \`virtual_aliases\` (
  \`id\` int(11) NOT NULL auto_increment,
  \`domain_id\` int(11) NOT NULL,
  \`source\` varchar(100) NOT NULL,
  \`destination\` varchar(100) NOT NULL,
  PRIMARY KEY (\`id\`),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" > table.sql
mysql --password="$mysql_root_pass" mailserver < table.sql
rm table.sql

echo -n "Souhaitez-vous créer l'adresse mail webmaster@$domain et son admin@$domain ? (Recommandé) [o/n]: "
read res
if [ "$res" = "o" ]
then
    ./new_mail.sh "webmaster@$domain" "$mysql_root_pass"
    ./new_alias.sh "admin@$domain" "webmaster@$domain" "$mysql_root_pass"
fi

# Config postfix for use MySQL
tmp="user = $mysql_mail_user
password = $mysql_mail_pass
hosts = 127.0.0.1
dbname = mailserver
query = SELECT 1 FROM virtual_domains WHERE name='%s'"
echo "$tmp" > /etc/postfix/mysql-virtual-mailbox-domains.cf
tmp="user = $mysql_mail_user
password = $mysql_mail_pass
hosts = 127.0.0.1
dbname = mailserver
query = SELECT 1 FROM virtual_users WHERE email='%s'"
echo "$tmp" > /etc/postfix/mysql-virtual-mailbox-maps.cf
tmp="user = $mysql_mail_user
password = $mysql_mail_pass
hosts = 127.0.0.1
dbname = mailserver
query = SELECT destination FROM virtual_aliases WHERE source='%s'"
echo "$tmp" > /etc/postfix/mysql-virtual-alias-maps.cf
service postfix restart

rm /etc/dovecot/conf.d/15-mailboxes.conf

main_cf=`cat ./conf_files/postfix/main.cf`
main_cf=`echo "$main_cf" | awk '{ sub("\[host\]",""defined_myhost"");print}' defined_myhost="$host"`
main_cf=`echo "$main_cf" | awk '{ sub("\[domain\]",""defined_domain"");print}' defined_domain="$domain"`
echo "$main_cf" > /etc/postfix/main.cf
cp -f ./conf_files/postfix/master.cf /etc/postfix/master.cf
cp -f ./conf_files/dovecot/dovecot.conf /etc/dovecot/dovecot.conf
cp -f ./conf_files/dovecot/10-mail.conf /etc/dovecot/conf.d/10-mail.conf
mkdir -p /var/mail/vhosts/$domain
groupadd -g 5000 vmail
useradd -g vmail -u 5000 vmail -d /var/mail
chown -R vmail:vmail /var/mail
cp -f ./conf_files/dovecot/10-auth.conf /etc/dovecot/conf.d/10-auth.conf
cp -f ./conf_files/dovecot/auth-sql.conf.ext /etc/dovecot/conf.d/auth-sql.conf.ext
dovecot_sql_conf=`cat ./conf_files/dovecot/dovecot-sql.conf.ext`
dovecot_sql_conf=`echo "$dovecot_sql_conf" | awk '{ sub("\[mysql_mail_user\]",""defined_mysql_user"");print}' defined_mysql_user="$mysql_mail_user"`
dovecot_sql_conf=`echo "$dovecot_sql_conf" | awk '{ sub("\[mysql_mail_pass\]",""defined_mysql_pass"");print}' defined_mysql_pass="$mysql_mail_pass"`
echo "$dovecot_sql_conf" > /etc/dovecot/dovecot-sql.conf.ext
chown -R vmail:dovecot /etc/dovecot
chmod -R o-rwx /etc/dovecot
cp -f ./conf_files/dovecot/10-master.conf /etc/dovecot/conf.d/10-master.conf
openssl req -new -x509 -days 1000 -nodes -out "/etc/dovecot/dovecot.pem" -keyout "/etc/dovecot/private/dovecot.pem"
cp -f ./conf_files/dovecot/10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf
service dovecot restart
service postfix restart
echo -n "Souhaitez-vous installer roundcube ? (Recommandé) [o/n]: "
read res
if [ "$res" = "o" ]
then
    ./install_roundcube.sh $domain $host $mysql_root_pass
fi