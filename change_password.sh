#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

if test -z "$1"
then
    echo -n "adresse mail (exemple@exemple.fr): "
    read email
else
    email=$1
fi

if test -z "$2"
then
    echo -n "Nouveau mot de passe: "
    read pass
else
    pass=$2
fi

echo "UPDATE \`mailserver\`.\`virtual_users\` SET password=ENCRYPT('$pass', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))) WHERE email='$email' LIMIT 1;" > data.sql
mysql -u root -p mailserver < data.sql
echo "Mot de passe modifié"