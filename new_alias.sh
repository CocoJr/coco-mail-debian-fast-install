#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

if test -z "$3"
then
    echo -n "Merci d'indiquez votre mot de passe super utilisateur root MySQL: "
    read root_pass
else
    root_pass=$3
fi

if test -z "$1"
then
    echo -n "Alias (exemple@exemple.fr): "
    read alias
else
    alias="$1"
fi

if test -z "$2"
then
    echo -n "SELECT email from \`mailserver\`.\`virtual_users\`;" > data.sql
    available=`mysql -u root --password="$root_pass" -N -B < data.sql`
    echo "Adresse email disponible:"
    i=0
    for mail in "$available"
    do
        if test "$i" -eq 0
        then
            lemail="$mail"
        fi
        echo " - $mail"
        i=1
    done
    echo -n "Mail de destination ($lemail): "
    read email
    if [ "$email" = "" ]
    then
        email="$lemail"
    fi
else
    email="$2"
fi

echo -n "SELECT domain_id from \`mailserver\`.\`virtual_users\` WHERE \`virtual_users\`.\`email\` = '$email';" > data.sql
domain=`mysql -u root --password="$root_pass" -N -B < data.sql`
echo "INSERT INTO \`mailserver\`.\`virtual_aliases\`
  (\`domain_id\`, \`source\`, \`destination\`)
VALUES
  ('$domain', '$alias', '$email');" > data.sql
mysql -u root --password="$root_pass" mailserver < data.sql
rm data.sql
echo "L'alias $alias pour l'adresse mail $email à bien été créé."