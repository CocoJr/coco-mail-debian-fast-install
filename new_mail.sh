#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

if test -z "$1"
then
    echo -n "adresse mail (exemple@exemple.fr): "
    read email
else
    email=$1
fi

if test -z "$2"
then
    echo -n "Merci d'indiquez votre mot de passe super utilisateur root MySQL: "
    read root_pass
else
    root_pass=$2
fi

echo "Création du compte $email"
echo -n "Choisir un mot de passe pour l'adresse mail \"$email\": "
read email_pass
domain=`echo $email | cut -f 2 -d "@"`
echo -n "SELECT * from \`mailserver\`.\`virtual_domains\` where \`name\` = '$domain';" > data.sql
tmp=`mysql -u root --password="$root_pass" mailserver -vv < data.sql`
tmp=`echo "$tmp" | grep -c "$domain"`
if test "$tmp" -eq 1
then
    echo "Le domaine $domain n'est pas enregistré comme domaine autorisé en base de donnée."
    echo -n "Merci d'indiquer le host (sous domaine) mail de votre domaine (exemple: mail): "
    read host
    echo "Ajout du domaine à la liste des domaines autorisés en base de données ..."
    echo "INSERT INTO \`mailserver\`.\`virtual_domains\`
          (\`id\` ,\`name\`)
        VALUES
          ('1', '$domain'),
          ('2', '$host.$domain'),
          ('3', '$host'),
          ('4', 'localhost.$domain');" > data.sql
    mysql -u root --password="$root_pass" mailserver < data.sql
    echo "Domaine ajouté"
fi
echo "INSERT INTO \`mailserver\`.\`virtual_users\`
  (\`id\`, \`domain_id\`, \`password\` , \`email\`)
VALUES
  ('1', '1', ENCRYPT('$email_pass', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), '$email');" > data.sql
mysql -u root --password="$root_pass" mailserver < data.sql
rm data.sql
echo "L'adresse mail $email à bien été créé."