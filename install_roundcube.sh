#!/bin/bash

if [[ "$USER" != 'root' ]]; then
	echo "Vous devez lancer ce script en tant que super utilisateur root"
	exit
fi

if test -z "$1"
then
    echo -n "Votre domaine (exemple: votredomaine.fr): "
    read domain
else
    domain=$1
fi

if test -z "$2"
then
    echo -n "Host (exemple: mail): "
    read host
else
    host=$2
fi

if test -z "$3"
then
    echo -n "Merci d'indiquez votre mot de passe super utilisateur root MySQL: "
    read root_pass
else
    root_pass=$3
fi

cd /var/www/
echo -n "Mot de passe de l'utilisateur roundcube: "
read round_pass
echo "Installation de roundcube ..."
wget https://downloads.sourceforge.net/project/roundcubemail/roundcubemail/1.1.2/roundcubemail-1.1.2-complete.tar.gz
tar -zxf roundcubemail-1.1.2-complete.tar.gz
rm roundcubemail-1.1.2-complete.tar.gz
chown -hR www-data: roundcubemail-1.1.2 && sudo chmod -R 775 roundcubemail-1.1.2
mv roundcubemail-1.1.2 $host
virtual_host="/etc/apache2/sites-available/$host.$domain.conf"
echo "<VirtualHost *:80>" > $virtual_host
echo "    ServerAdmin webmaster@$domain" >> $virtual_host
echo "    # Le nom de domaine associé de ce virtual host" >> $virtual_host
echo "    ServerName  $host.$domain" >> $virtual_host

echo "    # Racine" >> $virtual_host
echo "    DocumentRoot /var/www/$host/" >> $virtual_host

echo "    # Règle .htaccess" >> $virtual_host
echo "    <Directory /var/www/$host/>" >> $virtual_host
echo "        # On autorise tous le monde a voir le site" >> $virtual_host
echo "        Options -Indexes" >> $virtual_host
echo "        AllowOverride All" >> $virtual_host
echo "        Order allow,deny" >> $virtual_host
echo "        Allow from all" >> $virtual_host
echo "    </Directory>" >> $virtual_host

echo "    <LocationMatch \"/(config|temp|logs)/\">" >> $virtual_host
echo "      Order Deny,Allow" >> $virtual_host
echo "      Deny from All" >> $virtual_host
echo "    </LocationMatch>" >> $virtual_host

echo "    # Enregistrement des logs d'accès et d'erreurs" >> $virtual_host
echo "    ErrorLog /var/log/apache2/$host-errors.log" >> $virtual_host
echo "    TransferLog /var/log/apache2/$host-access.log" >> $virtual_host
echo "</VirtualHost>" >> $virtual_host
a2ensite "$host.$domain"
service apache2 reload
echo "CREATE DATABASE IF NOT EXISTS roundcubemail;
GRANT ALL PRIVILEGES ON roundcubemail.* TO roundcube@localhost IDENTIFIED BY \"$round_pass\";
FLUSH PRIVILEGES;" > data.sql
mysql -u root --password="$root_pass" < data.sql
rm data.sql
echo "Fin de l'installation de roundcube."
echo "Allez sur http://$host.$domain/installer/ pour finaliser celle-ci";